package ru.tsk.ilina.tm.api.repository;

import ru.tsk.ilina.tm.command.AbstractCommand;
import ru.tsk.ilina.tm.model.Command;

import java.util.Collection;

public interface ICommandRepository {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<AbstractCommand> getCommand();

    Collection<AbstractCommand> getArguments();

    Collection<String> getCommandName();

    Collection<String> getCommandArg();

    void add(AbstractCommand command);

}
