package ru.tsk.ilina.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.api.repository.IAbstractOwnerRepository;
import ru.tsk.ilina.tm.api.repository.IAbstractRepository;
import ru.tsk.ilina.tm.api.service.IAbstractOwnerService;
import ru.tsk.ilina.tm.exception.empty.EmptyIdException;
import ru.tsk.ilina.tm.exception.empty.EmptyUserIdException;
import ru.tsk.ilina.tm.model.AbstractOwnerEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity, R extends IAbstractOwnerRepository<E>> extends AbstractService<E, R> implements IAbstractOwnerService<E> {

    protected AbstractOwnerService(@NotNull final R repository) {
        super(repository);
    }

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    @Override
    public List<E> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(userId, comparator);
    }

    @Override
    public E removeByID(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        return repository.removeByID(userId, id);
    }

    @Override
    public E findByID(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        return repository.findByID(userId, id);
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final E entity) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        repository.add(userId, entity);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        repository.remove(userId, entity);
    }

}
