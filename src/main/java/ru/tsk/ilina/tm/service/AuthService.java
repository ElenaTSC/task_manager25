package ru.tsk.ilina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.api.service.IAuthService;
import ru.tsk.ilina.tm.api.service.IPropertyService;
import ru.tsk.ilina.tm.api.service.IUserService;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.exception.user.AccessDeniedException;
import ru.tsk.ilina.tm.exception.empty.EmptyIdException;
import ru.tsk.ilina.tm.exception.empty.EmptyLoginException;
import ru.tsk.ilina.tm.exception.empty.EmptyPasswordException;
import ru.tsk.ilina.tm.model.User;
import ru.tsk.ilina.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    private final IUserService userService;
    private final IPropertyService propertyService;

    private String userId;

    public AuthService(@NotNull final IUserService userService, @NotNull final PropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public void login(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = userService.findByLogin(login);
        if (user.isLocked()) throw new AccessDeniedException();
        @NotNull final String hash = HashUtil.salt(propertyService, password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void registry(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        userService.addUser(login, password, email);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        @NotNull final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public void updateUser(@NotNull final String userId, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) {
        userService.updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public void setPassword(@NotNull final String userId, @NotNull final String password) {
        userService.setPassword(userId, password);
    }

    public boolean isAuth() {
        return userId == null;
    }

    public void checkRoles(@Nullable final Role... roles) {
        if (roles == null || roles.length == 0) return;
        @Nullable final User user = getUser();
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

}
