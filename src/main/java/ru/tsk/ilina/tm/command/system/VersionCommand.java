package ru.tsk.ilina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version.";
    }

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println(serviceLocator.getPropertyService().getApplicationVersion());
    }

}
