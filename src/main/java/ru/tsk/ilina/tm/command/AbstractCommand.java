package ru.tsk.ilina.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.api.service.IServiceLocator;
import ru.tsk.ilina.tm.enumerated.Role;

@Getter
@Setter
public abstract class AbstractCommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @NotNull
    public abstract String name();

    @NotNull
    public abstract String description();

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract void execute();

    @Nullable
    public Role[] roles() {
        return null;
    }

    @NotNull
    @Override
    public String toString() {
        String result = "";
        String name = name();
        String description = description();
        String arg = arg();
        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += "[" + arg + "]";
        if (description != null && !description.isEmpty()) result += "[" + description + "]";
        return result;
    }

}
