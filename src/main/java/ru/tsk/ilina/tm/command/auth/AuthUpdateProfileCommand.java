package ru.tsk.ilina.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.api.service.IUserService;
import ru.tsk.ilina.tm.command.AbstractAuthUserCommand;
import ru.tsk.ilina.tm.util.TerminalUtil;

public class AuthUpdateProfileCommand extends AbstractAuthUserCommand {

    @NotNull
    @Override
    public String name() {
        return "update";
    }

    @NotNull
    @Override
    public String description() {
        return "Update information about user";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME: ");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME: ");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME: ");
        @NotNull final String middleName = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updateUser(userId, firstName, lastName, middleName);
        System.out.println("[OK]");
    }

}
