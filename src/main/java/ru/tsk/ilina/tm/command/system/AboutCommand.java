package ru.tsk.ilina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer info.";
    }

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println(serviceLocator.getPropertyService().getDeveloper());
        System.out.println(serviceLocator.getPropertyService().getEmail());
    }

}
