package ru.tsk.ilina.tm.repository;

import lombok.NoArgsConstructor;
import ru.tsk.ilina.tm.api.repository.IProjectRepository;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@NoArgsConstructor
public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

}
