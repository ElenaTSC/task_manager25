package ru.tsk.ilina.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.api.repository.IAbstractRepository;
import ru.tsk.ilina.tm.model.AbstractEntity;
import ru.tsk.ilina.tm.model.User;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractRepository<E extends AbstractEntity> implements IAbstractRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Nullable
    @Override
    public List<E> findAll() {
        return entities;
    }

    @NotNull
    @Override
    public E add(@NotNull final E entity) {
        entities.add(entity);
        return entity;
    }

    @Nullable
    @Override
    public E findById(@NotNull final String id) {
        for (@NotNull final E entity : entities) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        @NotNull final E entity = findById(id);
        if (entity == null) return null;
        return remove(entity);
    }

    @Nullable
    @Override
    public E remove(@NotNull final E entity) {
        entities.remove(entity);
        return entity;
    }

}
